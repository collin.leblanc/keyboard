#include <Keypad.h>
#include "keycodes.h"

const byte ROWS = 5;
const byte COLS = 12; 

//First layer keymap
char keysAlpha[ROWS][COLS] = {
    { KEY_1,            KEY_2,        KEY_3,        KEY_4,      KEY_5,       KEY_6,     KEY_7,     KEY_8,       KEY_9,      KEY_0,       KEY_DASH,      KEY_EQUAL      },
    { KEY_TAB,          KEY_Q,        KEY_W,        KEY_E,      KEY_R,       KEY_T,     KEY_Y,     KEY_U,       KEY_I,      KEY_O,       KEY_P,         KEY_BACK       },
    { KEY_CAPS_LOCK,    KEY_A,        KEY_S,        KEY_D,      KEY_F,       KEY_G,     KEY_H,     KEY_J,       KEY_K,      KEY_L,       KEY_SEMICOLON, KEY_APOSTROPHE },
    { KEY_L_SHIFT,      KEY_Z,        KEY_X,        KEY_C,      KEY_V,       KEY_B,     KEY_N,     KEY_M,       KEY_COMMA,  KEY_PERIOD,  KEY_SLASH,     KEY_R_SHIFT    },
    { KEY_L_CTRL,       KEY_L_ALT,    KEY_L_GUI,    KEY_FN,     KEY_LBRACK,  KEY_SPACE, KEY_SPACE, KEY_RBRACK,  KEY_DELETE, KEY_HOME,    KEY_END,       KEY_ENTER      }
};

//Second layer keymap
char keysFunc[ROWS][COLS] = {
    { KEY_F1,           KEY_F2,       KEY_F3,       KEY_F4,     KEY_F5,      KEY_F6,     KEY_F7,     KEY_F8,          KEY_F9,                 KEY_F10,          KEY_F11,          KEY_F12        },
    { NO_KEY,           NO_KEY,       NO_KEY,       NO_KEY,     NO_KEY,      NO_KEY,     NO_KEY,     NO_KEY,          KEY_UP_ARROW,           NO_KEY,           NO_KEY,           KEY_BACK       },
    { NO_KEY,           NO_KEY,       NO_KEY,       NO_KEY,     NO_KEY,      NO_KEY,     NO_KEY,     KEY_LEFT_ARROW,  KEY_DOWN_ARROW,         KEY_RIGHT_ARROW,  NO_KEY,           KEY_APOSTROPHE },
    { KEY_L_SHIFT,      NO_KEY,       NO_KEY,       NO_KEY,     NO_KEY,      NO_KEY,     NO_KEY,     NO_KEY,          KEY_LEFT_ARROW,         KEY_DOWN_ARROW,   KEY_RIGHT_ARROW,  KEY_R_SHIFT    },
    { KEY_L_CTRL,       KEY_L_ALT,    KEY_L_GUI,    KEY_FN,     KEY_LBRACK,  KEY_SPACE,  KEY_SPACE,  KEY_RBRACK,      KEY_DELETE,             KEY_HOME,         KEY_END,          KEY_ENTER      },
};

//PCB is not wired correctly. Columns on Teensy are assigned to rowPins, Rows are assigned to colPins.
byte rowPins[COLS] = {12,11,10,9,8,7,6,5,4,3,2,1};
byte colPins[ROWS] = {14,15,16,17,18};

//Holds the corrected keymaps
char keymapLayer1[COLS][ROWS];
char keymapLayer2[COLS][ROWS];

//initialize an instance of class NewKeypad
Keypad layerAlpha = Keypad( makeKeymap(keysAlpha), rowPins, colPins, COLS, ROWS);
Keypad layerFunc = Keypad( makeKeymap(keysFunc), rowPins, colPins, COLS, ROWS);
Keypad currentLayer = Keypad();

//Variable for multikey
char pressedKeys[6];
char pressedModifiers[6];

//Variables for layer control
bool functionPressed = false;
bool lastState = false;

void setup(){
    Serial.begin(57600);

    // Blink LED to confirm successful upload.
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    delay(500);
    digitalWrite(13, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    delay(1000);

    //Convert keymap from above Real layout to expected flipped and rotated layout.
    ConvertKeyMap(1, keysAlpha);
    ConvertKeyMap(2, keysFunc);

    //Create Keypads with each keymap
    layerAlpha = Keypad( makeKeymap(keymapLayer1), rowPins, colPins, COLS, ROWS);
    layerFunc = Keypad( makeKeymap(keymapLayer2), rowPins, colPins, COLS, ROWS);

    //Create Keypad to hold currently selected layer
    currentLayer = Keypad( makeKeymap(keymapLayer1), rowPins, colPins, COLS, ROWS);
}

void loop(){

    //Check if Function key has been pressed again
    if (functionPressed && functionPressed != lastState){
        //Switch to Function layer
        currentLayer = layerFunc;
        lastState = functionPressed;
    }
    else if (functionPressed == false && functionPressed != lastState) {
        //Switch to Alpha layer
        currentLayer = layerAlpha;
        lastState = functionPressed;
    }

    if (currentLayer.getKeys()){
            for (int i = 0; i <= 6; i ++){
                switch (currentLayer.key[i].kstate){
                    case PRESSED:
                        //Check to see if Modifier keys are being pressed
                        if (currentLayer.key[i].kchar == KEY_L_CTRL || currentLayer.key[i].kchar == KEY_L_SHIFT || currentLayer.key[i].kchar == KEY_L_ALT || currentLayer.key[i].kchar == KEY_L_GUI || currentLayer.key[i].kchar == KEY_R_SHIFT){
                            pressedModifiers[i] = currentLayer.key[i].kchar;
                            break;
                        }
                        else {
                            pressedKeys[i] = currentLayer.key[i].kchar;
                            break;
                        }
                    case HOLD: {
                        pressedKeys[i] = currentLayer.key[i].kchar;
                        break;
                    }
                    case RELEASED: {
                        //Check to see if Function key has been released
                        if (currentLayer.key[i].kchar == KEY_FN)
                        {
                            //Toggle layers
                            functionPressed = !functionPressed;
                        }

                        //Clear lists of pressed keys
                        pressedKeys[i] = 0; 
                        pressedModifiers[i] = 0;
                        break;
                    }
                }
            }
    }

    //Reset Modifier Keys
    Keyboard.set_modifier(0);

    //Set the currently pressed keys to the keyboard's queue
    for (int key = 0; key <= 6; key++){
        setKeys(pressedKeys[key], key);
    }

    //Type currently pressed keys
    Keyboard.send_now();
}

void setKeys(int k, int i){
    //Go through possible Modifier key combinations
    switch(pressedModifiers[0]){
        case KEY_L_CTRL: 
            if (pressedModifiers[1] == KEY_L_SHIFT | pressedModifiers[1] == KEY_R_SHIFT){
                Keyboard.set_modifier(MODIFIERKEY_CTRL | MODIFIERKEY_SHIFT); break;
            }
            else if (pressedModifiers[1] == KEY_L_ALT){
                Keyboard.set_modifier(MODIFIERKEY_CTRL | MODIFIERKEY_ALT); break;
            }
            else {
                Keyboard.set_modifier(MODIFIERKEY_CTRL); break;
            }
        case KEY_L_ALT:
            if (pressedModifiers[1] == KEY_L_SHIFT | pressedModifiers[1] == KEY_R_SHIFT) {
                Keyboard.set_modifier(MODIFIERKEY_ALT | MODIFIERKEY_SHIFT); break;
            }
            else {
                Keyboard.set_modifier(MODIFIERKEY_ALT); break;
            }
        case KEY_L_SHIFT:
            if (pressedModifiers[1] == KEY_L_CTRL){
                Keyboard.set_modifier(MODIFIERKEY_CTRL | MODIFIERKEY_SHIFT); break;
            }
            if (pressedModifiers[1] == 0) {
                Keyboard.set_modifier(MODIFIERKEY_SHIFT); break;
            }
        case KEY_R_SHIFT:
            if (pressedModifiers[1] == KEY_L_CTRL){
                Keyboard.set_modifier(MODIFIERKEY_CTRL | MODIFIERKEY_SHIFT); break;
            }
            if (pressedModifiers[1] == 0) {
                Keyboard.set_modifier(MODIFIERKEY_SHIFT); break;
            }
        case KEY_L_GUI:
            if (pressedModifiers[1] == 0) {
                Keyboard.set_modifier(MODIFIERKEY_GUI); break;
            }
    }
    
    //Go through list of pressed keys and assign them to the keyboard's queue
    switch(k){
    default:
      switch(i){
        case 0: Keyboard.set_key1(pressedKeys[i]); Serial.println("Key 1 :" + String(pressedKeys[i])); break;
        case 1: Keyboard.set_key2(pressedKeys[i]); Serial.println("Key 2 :" + String(pressedKeys[i])); break;
        case 2: Keyboard.set_key3(pressedKeys[i]); Serial.println("Key 3 :" + String(pressedKeys[i])); break;
        case 3: Keyboard.set_key4(pressedKeys[i]); Serial.println("Key 4 :" + String(pressedKeys[i])); break;
        case 4: Keyboard.set_key5(pressedKeys[i]); Serial.println("Key 5 :" + String(pressedKeys[i])); break;
        case 5: Keyboard.set_key6(pressedKeys[i]); Serial.println("Key 6 :" + String(pressedKeys[i])); break;
      }
      
      break;
  }

}

// Needed to fix incorrect wiring of PCB
void ConvertKeyMap(int layer, char keys[ROWS][COLS]){ 
    if (layer == 1){
        for (int c = 0; c <= COLS-1; c++){
            for (int r = 0; r <= ROWS-1; r++){
                keymapLayer1[c][r] = keys[r][c];
            }
        }
    }
    else if (layer == 2){
        for (int c = 0; c <= COLS-1; c++){
            for (int r = 0; r <= ROWS-1; r++){
                keymapLayer2[c][r] = keys[r][c];
            }
        }
    }
}
